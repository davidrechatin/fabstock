from django.shortcuts import render
from .models import Stock
from .forms import FilterForm1, FilterForm2, TriForm
from django.core.paginator import Paginator, EmptyPage

# Create your views here.


def liste_stock(request):
    if 'filtre' not in request.session:
        request.session['filtre'] = {}
    if 'tri' not in request.session:
        request.session['tri'] = 'date_entree'
    if 'etat1' not in request.session:
        request.session['etat1'] = {}
    if 'etat2' not in request.session:
        request.session['etat2'] = {}
    if 'etat3' not in request.session:
        request.session['etat3'] = {}
    if 'affich' not in request.session:
            request.session['affich'] = '10'

    if request.method == "POST":
            form1 = FilterForm1(request.POST)
            form2 = FilterForm2(request.POST)
            tri = TriForm(request.POST)
            request.session['affich'] = request.POST.get('nb_par_page')
            dict_rech = {}
            if form1.is_valid():
                request.session['etat1'] = filtres1 = form1.cleaned_data
                for filtre, valeur in filtres1.items():
                    if valeur != "" and valeur is not None:
                        if filtre == "categorie" or filtre == "type" or filtre == "acces" or filtre == "proprietaire":
                            cle = filtre+"__libelle"
                        else:
                            cle = filtre+"__icontains"
                        dict_rech[cle] = valeur
#                        print("La clé1 {} => {}.".format(cle, valeur))
            if form2.is_valid():
                request.session['etat2'] = filtres2 = form2.cleaned_data
                for filtre, valeur in filtres2.items():
                    if valeur != "" and valeur is not None:
                        if filtre == "categorie" or filtre == "type" or filtre == "acces" or filtre == "proprietaire":
                            cle = filtre+"__libelle"
                        else:
                            cle = filtre+"__icontains"
                        dict_rech[cle] = valeur
            if tri.is_valid():
                request.session['etat3'] = choix = tri.cleaned_data
                choix_tri = choix['sens']+choix['tri']
                request.session['tri'] = choix_tri
            request.session['filtre'] = dict_rech
    else:
        if request.GET.get('page') is None:
            request.session['filtre'] = {}
            request.session['tri'] = 'date_entree'
            request.session['etat1'] = {}
            request.session['etat2'] = {}
            request.session['etat3'] = {}
    #print("*****", request.session.keys())
    liste = Stock.objects.filter(**request.session['filtre']).order_by(request.session['tri'])
    compte = liste.count()
    paginator = Paginator(liste, request.session['affich'])
    page = request.GET.get('page')
    if page is None:
        page = 1
    else:
        page = int(page)
    try:
        liste = paginator.page(page)
    except EmptyPage:
        liste = paginator.page(paginator.num_pages)
    form1 = FilterForm1(initial=request.session['etat1'])
    form2 = FilterForm2(initial=request.session['etat2'])
    form3 = TriForm(initial=request.session['etat3'])
    return render(request,
                  'stock/stock_list.html',
                  {'liste': liste,
                   'form1': form1,
                   'form2': form2,
                   'form3': form3,
                   'affichage': request.session['affich'],
                   'compte' : compte
                   })
