from django.contrib import admin
from .models import Stock, Categorie, Proprietaire, Type, Acces

# Register your models here.


class StockAdmin(admin.ModelAdmin):
    list_display = ('designation', 'proprietaire', 'etat', 'prix_vente')
    list_filter = ('categorie', 'type', 'acces', 'marque', 'proprietaire', 'url_fournisseur', )
    date_hierarchy = 'date_entree'
    ordering = ('date_entree', )
    search_fields = ('designation', 'modele')

    fieldsets = (
        # Fieldset 1 : champs principaux
        ('Champs principaux', {
            'classes': ['wide', ],
            'fields': ('date_entree', 'categorie', 'type', 'acces', 'quantite', 'designation', 'prix_vente')
        }),
        # Fieldset 2 : champs secondaires
        ('Champs secondaires', {
            'classes': ['collapse', ],
            'fields': ('marque', 'modele', 'proprietaire', 'etat', 'date_inventaire', 'commentaire', 'prix_achat', 'url_fournisseur')
        }),
    )


admin.site.register(Stock, StockAdmin)
admin.site.register(Categorie)
admin.site.register(Proprietaire)
admin.site.register(Type)
admin.site.register(Acces)