# -*- coding: utf-8 -*-
# Generated by Django 1.10.5 on 2017-01-24 15:12
from __future__ import unicode_literals

import datetime
from django.db import migrations, models
from django.utils.timezone import utc


class Migration(migrations.Migration):

    dependencies = [
        ('stock', '0003_auto_20170124_1609'),
    ]

    operations = [
        migrations.AlterField(
            model_name='stock',
            name='date_entree',
            field=models.DateTimeField(default=datetime.datetime(2017, 1, 24, 15, 12, 9, 582487, tzinfo=utc)),
        ),
        migrations.AlterField(
            model_name='stock',
            name='prix_achat',
            field=models.FloatField(blank=True, null=True),
        ),
        migrations.AlterField(
            model_name='stock',
            name='prix_vente',
            field=models.FloatField(blank=True, null=True),
        ),
    ]
