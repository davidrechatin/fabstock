from django import forms
from .models import Stock, Type, Proprietaire, Acces, Categorie


class FilterForm1(forms.ModelForm):

    liste11 = Categorie.objects.all()
    choix11 = [('', '---------')]
    for truc in list(liste11):
        choix11.append((truc.libelle, truc.libelle))
    categorie = forms.ChoiceField(choix11, required=False)

    liste12 = Acces.objects.all()
    choix12 = [('', '---------')]
    for truc in list(liste12):
        choix12.append((truc.libelle, truc.libelle))
    acces = forms.ChoiceField(choix12, required=False)

    field_order = ('categorie', 'acces', 'designation', 'marque', 'modele')

    class Meta:
        model = Stock
        fields = (
            'designation',
            'marque',
            'modele'
        )


class FilterForm2(forms.ModelForm):

    liste21 = Type.objects.all()
    choix21 = [('', '---------')]
    for truc in list(liste21):
        choix21.append((truc.libelle, truc.libelle))
    type = forms.ChoiceField(choix21, required=False)

    liste22 = Proprietaire.objects.all()
    choix22 = [('', '---------')]
    for truc in list(liste22):
        choix22.append((truc.libelle, truc.libelle))
    proprietaire = forms.ChoiceField(choix22, required=False)

    field_order = ('type', 'proprietaire', 'etat', 'commentaire', 'url_fournisseur')

    class Meta:
        model = Stock
        fields = (
            'etat',
            'commentaire',
            'url_fournisseur'
        )


class TriForm(forms.ModelForm):

    CHOICES = (
        ('designation', 'designation'),
        ('categorie', 'categorie'),
        ('type', 'type'),
        ('acces', 'acces'),
        ('prix_vente', 'prix_vente'),
        ('quantite', 'quantite'),
        ('date_entree', 'date_entree'),
        ('proprietaire', 'proprietaire'),
        ('prix_achat', 'prix_achat'),
        ('marque', 'marque'),
        ('modele', 'modele'),
        ('etat', 'etat'),
        ('date_inventaire', 'date_inventaire'),
        ('commentaire', 'commentaire'),
        ('url_fournisseur', 'url_fournisseur')
    )

    DIRECTION = (
        ('', 'descendant'),
        ('-', 'ascendant')
    )

    tri = forms.ChoiceField(CHOICES)
    sens = forms.ChoiceField(DIRECTION, required=False)

    class Meta:
        model = Stock
        fields = ()
